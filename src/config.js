export const GOOGLE_MAPS_URL = 'https://maps.googleapis.com/maps/api/js'
export const GOOGLE_MAPS_API_KEY = process.env.NODE_ENV === 'production'
  ? 'AIzaSyDPmQaQVhN75K_O7woVl6UCrJwLaOZKtck'
  : 'AIzaSyAglrX5nwyWUkWdRkHlybhC3dvmANrB3_8'
export const GOOGLE_MAPS_START_ICON = 'https://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png'

export const GOOGLE_MAPS_ROUTE_DATA_URL = 'https://pexea12-co2-static-files.s3.eu-west-2.amazonaws.com/20_points.json'


export const mapCenter = {
  lat: 60.1941,
  lng: 24.8704,
}

export const points = {
  0: {
    id: 0,
    lat: 60.1699,
    lng: 24.9384,
    name: 'Rautatientori',
    dist: {},
  },
  1: {
    id: 1,
    lat: 60.1867,
    lng: 24.8277,
    name: 'Aalto University',
    dist: {},
  },
  2: {
    id: 2,
    lat: 60.2027,
    lng: 24.9577,
    name: 'Kumpula campus, University of Helsinki',
    dist: {},
  },
  3: {
    id: 3,
    lat: 60.1704,
    lng: 24.9522,
    name: 'Helsinki Cathedral',
    dist: {},
  },
  4: {
    id: 4,
    lat: 60.2131,
    lng: 24.8897,
    name: 'Messukeskus',
    dist: {},
  },
  5: {
    id: 5,
    dist: {},
    lat: 60.1677,
    lng: 24.9537,
    name: 'Market Square',
  },
  6: {
    id: 6,
    dist: {},
    name: 'Rock Church',
    lat: 60.173,
    lng: 24.9252,
  },
  7: {
    id: 7,
    name: 'Kampi',
    dist: {},
    lat: 60.1676,
    lng: 24.9302,
  },
  8: {
    id: 8,
    dist: {},
    name: 'Helsinki Ice Hall',
    lat: 60.1893,
    lng: 24.9225,
  },
  9: {
    id: 9,
    dist: {},
    name: 'Sibelius Monument and Park',
    lat: 60.1758,
    lng: 24.9081,
  },
  10: {
    id: 10,
    name: 'Oodi, Helsinki Central Library',
    lat: 60.1738,
    lng: 24.9381,
    dist: {},
  },
  11: {
    id: 11,
    name: 'Pasila station',
    lat: 60.1989,
    lng: 24.9336,
    dist: {},
  },
  12: {
    id: 12,
    name: 'Amos Rex',
    lat: 60.1706376,
    lng: 24.9339261,
    dist: {},
  },
  13: {
    id: 13,
    name: 'Linnanmäki Amusement Park',
    lat: 60.1852394,
    lng: 24.9058621,
    dist: {},
  },
  14: {
    id: 14,
    name: 'Allas Sea Pool',
    lat: 60.2136705,
    lng: 24.6474742,
    dist: {},
  },
  15: {
    id: 15,
    name: 'Kannelmäki station',
    lat: 60.2395663,
    lng: 24.8751317,
    dist: {},
  },
  16: {
    id: 16,
    name: 'S-market Maunula',
    lat: 60.2310746,
    lng: 24.9328697,
    dist: {},
  },
  17: {
    id: 17,
    name: 'Arabia 135',
    lat: 60.213632,
    lng: 24.9507861,
    dist: {},
  },
  18: {
    id: 18,
    name: 'Sello',
    lat: 60.2182227,
    lng: 24.7987842,
    dist: {},
  },
  19: {
    id: 19,
    name: 'Kaarelanpuisto',
    lat: 60.2473894,
    lng: 24.8063471,
    dist: {},
  },
  20: {
    id: 20,
    name: 'IKEA Espoo',
    lat: 60.2136705,
    lng: 24.6474742,
    dist: {},
  },
}



;(function(console){
  console.save = function(data, filename) {
    if(!data) {
      console.error('Console.save: No data')
      return;
    }

    if(!filename) filename = 'console.json'
    if(typeof data === "object"){
      data = JSON.stringify(data)
    }

    const blob = new Blob([data], {type: 'text/json'})
    const e = document.createEvent('MouseEvents')
    const a = document.createElement('a')

    a.download = filename
    a.href = window.URL.createObjectURL(blob)
    a.dataset.downloadurl = ['text/json', a.download, a.href].join(':')
    e.initMouseEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null)
    a.dispatchEvent(e)
  }
})(console)
