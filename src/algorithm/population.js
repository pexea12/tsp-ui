import shuffle from 'lodash.shuffle'



export const initPopulation = (routeOriginId, populationSize, points) => {
  const route = [ ]
  const pointIds = Object.keys(points)
  for (let i = 0; i < pointIds.length; i += 1) {
    if (Number(pointIds[i]) !== routeOriginId)
      route.push(Number(pointIds[i]))
  }

  const population = [
    [ routeOriginId, ...route ],
  ]

  for (let i = 1; i < populationSize; i += 1) {
    population.push([
      routeOriginId,
      ...shuffle(route)
    ])
  }

  return population
}

const distance = (originId, destId, points) => {
  if (originId === destId) return 0
  return points[originId].dist[destId].routes[0].legs[0].distance.value
}

export const routeLength = (route, points) => {
  const length = route.reduce((sum, pointId, index, array) => {
    return sum + distance(array[index - 1], pointId, points)
  })

  return distance(route[route.length - 1], route[0], points) + length
}

export const bestRoute = (population, points) => {
  let winner = null
  let maxScore = 0

  for (let i = 0; i < population.length; i += 1) {
    const score = 1 / (routeLength(population[i], points) + 0.1)
    if (score > maxScore) {
      maxScore = score
      winner = population[i]
    }
  }

  return {
    winner,
    routeLength: 1 / maxScore - 0.1,
  }
}

const tournamentWinner = (population, options, points) => {
  let maxScore = 0
  let winner = null

  for (let i = 0; i < options.tournamentSize; i += 1) {
    const j = Math.floor(Math.random() * population.length)
    const score = 1 / (routeLength(population[j], points) + 0.1);
    if (score > maxScore) {
      maxScore = score
      winner = population[j]
    }
  }

  return winner
}

const crossover = (dad, mom) => {
  let start = Math.floor(Math.random() * (dad.length - 1)) + 1
  let end = Math.floor(Math.random() * (mom.length - 1)) + 1
  start = start > end ? end : start
  end = start > end ? start : end

  const child = Array(dad.length)
  child[0] = dad[0]
  const marked = {}

  for (let i = start; i <= end; i += 1) {
    child[i] = dad[i]
    marked[dad[i]] = true
  }

  let j = 1
  for (let i = 1; i < mom.length; i += 1) {
    if (j == start) j = end + 1;
    if (!marked[mom[i]]) {
      child[j] = mom[i]
      j += 1
    }

  }

  return child
}

const mutate = (child, options) => {
  for (let i = 1; i < child.length; i += 1) {
    const coinToss = Math.random()
    if (coinToss < options.mutationRate) {
      const j = Math.floor(Math.random() * (child.length - 1)) + 1

      const t = child[i]
      child[i] = child[j]
      child[j] = t
    }
  }

  return child
}

export const evolvePopulation = (population, options, points) => {
  const { winner } = bestRoute(population, points)
  const newPopulation = [ winner ]

  for (let i = 1; i < population.length; i += 1) {
    const dad = tournamentWinner(population, options, points)
    const mom = tournamentWinner(population, options, points)

    const child = crossover(dad, mom)
    newPopulation.push(mutate(child, options))
  }

  return newPopulation
}
