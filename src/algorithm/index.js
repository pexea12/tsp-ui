import {
  initPopulation,
  evolvePopulation,
  bestRoute,
} from './population'



export const findShortestRoute = (routeOriginId, points, customOptions = {}, self) => {
  const options = {
    steps: 1000,
    tournamentSize: 20,
    mutationRate: 0.02,
    populationSize: 100,
    ...customOptions,
  }

  let winnerInfo

  let population = initPopulation(routeOriginId, options.populationSize, points)

  for (let step = 0; step < options.steps; step += 1)  {
    population = evolvePopulation(
      population,
      options,
      points,
    )

    winnerInfo = bestRoute(population, points)
    if (step % 100 === 0) {
      self.postMessage({
        type: 'updateLog',
        log: winnerInfo,
      })
    }
  }

  self.postMessage({
    type: 'done',
    winner: winnerInfo,
  })

  return winnerInfo
}
