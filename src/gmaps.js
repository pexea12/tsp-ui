import {
  GOOGLE_MAPS_URL,
  GOOGLE_MAPS_API_KEY,
} from './config'


let initialized = !!window.google

let resolveInitPromise
let rejectInitPromise


// This promise handles the initialization
// status of the google maps script.
const initPromise = new Promise((resolve, reject) => {
  resolveInitPromise = resolve
  rejectInitPromise = reject
})

export const initGmaps = () => {
  // If Google Maps already is initialized
  // the `initPromise` should get resolved
  // eventually.
  if (initialized) return initPromise

  initialized = true
  // The callback function is called by
  // the Google Maps script if it is
  // successfully loaded.
  window.gmapsCallback = () => resolveInitPromise(window.google)

  const script = document.createElement('script')
  script.async = true
  script.defer = true
  script.src = `${GOOGLE_MAPS_URL}?key=${GOOGLE_MAPS_API_KEY}&callback=gmapsCallback`
  script.onerror = rejectInitPromise
  document.querySelector('head').appendChild(script)

  return initPromise;
}













