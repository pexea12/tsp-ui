import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'

import {
  points,
} from '../config'


Vue.use(Vuex)

const persistor = new VuexPersistence({
  storage: window.localStorage,
  reducer: () => ({
  }),
})

const store = new Vuex.Store({
  state: {
    points,
    winnerInfo: null,
    loading: false,
    processing: false,
    logs: [],
    routeOriginId: null,
    showRoute: true,
  },
  mutations: {
    addPoint(state, point) {
      state.points.push(point)
    },

    setPoints(state, points) {
      Vue.set(state, 'points', points)
    },

    updateDist(state, payload) {
      const {
        originId,
        destId,
        routeRes,
      } = payload

      if (!(originId in state.points) || !(destId in state.points))
        return console.error(`Point ID ${originId} or ${destId} not in point list`)

      Vue.set(state.points[originId].dist, destId, routeRes)
    },

    setLoading(state, loading) {
      Vue.set(state, 'loading', loading)
    },

    setProcessing(state, processing) {
      Vue.set(state, 'processing', processing)
    },

    setWinnerInfo(state, winnerInfo) {
      Vue.set(state, 'winnerInfo', winnerInfo)
    },

    clearLogs(state) {
      Vue.set(state, 'logs', [])
    },

    pushLog(state, log) {
      state.logs.push(log)
    },

    setRouteOriginId(state, routeOriginId) {
      Vue.set(state, 'routeOriginId', routeOriginId)
    },

    setShowRoute(state, showRoute) {
      Vue.set(state, 'showRoute', showRoute)
    },
  },
  plugins: [
    persistor.plugin,
  ],
})


export default store
