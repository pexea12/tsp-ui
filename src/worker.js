import { findShortestRoute } from './algorithm'


addEventListener('message', ({ data }) => {
  if (data.type === 'calculate') {
    findShortestRoute(data.routeOriginId, data.points, data.customOptions, self)
  }
})

